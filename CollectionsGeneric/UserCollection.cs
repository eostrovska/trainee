﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace CollectionsGeneric
{
    class UserCollection<T> : IEnumerable<T>, IEnumerator<T>
    {
        readonly T[] elements = new T[4];
        public T this[int index]
        {
            get { return elements[index]; }
            set { elements[index] = value; }
        }
        int position = -1;

        bool IEnumerator.MoveNext()
        {
            if(position < elements.Length - 1)
            {
                position++;
                return true;
            }
            return false;
        }
        void IEnumerator.Reset()
        {
            position = -1;
        }
        object IEnumerator.Current
        {
            get { return elements[position]; }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this;
        }
        T IEnumerator<T>.Current
        {
            get { return elements[position]; }
        }
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return this;
        }
        void IDisposable.Dispose()
        {
            ((IEnumerator)this).Reset();
        }
    }
}
