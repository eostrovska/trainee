﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace CollectionsGeneric
{
    class Program
    {
        static void Main()
        {
            var collection = new UserCollection<Elements>();
            collection[0] = new Elements(12, 12, 12);
            collection[1] = new Elements(62, 52, 16);
            collection[2] = new Elements(62, 32, 32);
            collection[3] = new Elements(124, 412, 42);
            //collection[4] = new Elements(12, 12, 12);
            foreach(Elements element in collection)
            {
                Console.WriteLine("{0}, {1}, {2}", element.Field1, element.Field2, element.Field3);
            }
            Console.ReadKey();
        }
    }
}
