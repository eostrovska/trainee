﻿using System;
using System.Collections;

namespace Less17_task05
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (dynamic item in MyMethod())
            {
                Console.WriteLine("{0}\t{1}", item.key, item.value);
            }

            Console.ReadKey();
        }

        static IEnumerable MyMethod()
        {
            yield return new { key = "hello", value = "world" };
            yield return new { key = "hi", value = "all" };
            yield return new { key = 15.6, values = 184 };
        }
    }
}
