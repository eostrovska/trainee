﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LambdaCalculusAndPredicate
{
    class Program
    {
        static void Main(string[] args)
        {
            //closure through local virable number
            int number = 9;
            Func<double, double> expression = x => x / 2;
            Console.WriteLine("Result : {0}", expression(number));

            //predicate
            IEnumerable<string> members = new List<string>
            {
                "One - First",
                "Two - Second",
                "Three - Third",
                "Eleven - Elevens"
            };
            WriteStream(members,"two",(x, y) => x.ToLower().StartsWith(y));
            WriteStream(members, "T", (x,y) => x.Contains(y));
            Console.ReadKey();
        }
        static void WriteStream(IEnumerable<string> members, string name, Func<string, string, bool> predicate)
        {
            foreach(string member in members)
            {
                if(predicate(member,name))
                {
                    Console.WriteLine("Result : {0}", member);
                }
            }
        }
    }
}
