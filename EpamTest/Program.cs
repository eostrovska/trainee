﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTest
{
    class Program
    {
        public void writeNum(int [] array)
        {
            int firstNum = array[0], secondNum = array[0], firstNumPosition = 0, secondNumPosition = 0;
            
            for(int i = 0; i < array.Length; i ++)
            {
                
                if (firstNum > array[i] && array[i] < 0)
                    {
                    secondNum = firstNum;
                    firstNum = array[i];
                    firstNumPosition = i;
                    secondNumPosition = Array.IndexOf(array, secondNum);
                    }               
            }
            Console.WriteLine("firstNum - {0}, secondNum - {1}, firstNumPosition - {2}, secondNumPosition - {3}", firstNum, secondNum, firstNumPosition, secondNumPosition);
        }

        static void Main(string[] args)
        {
            Program instanse = new Program();
            instanse.writeNum(new int[] { -1, -2, -5, 5, 7, -56 });
            Console.ReadKey();
        }
    }
}
