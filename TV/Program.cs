﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TV
{
    class TVset
    {
        protected internal int currentСhannel = 1;
        protected internal int numberOfChannel = 1;
        public void SwitchNext()
        {
            currentСhannel++;
            if (currentСhannel == 101)
            {
                currentСhannel = 1;
            }
            Console.WriteLine(currentСhannel);
        }
        public void SwitchPrevious()
        {
            currentСhannel--;
            if (currentСhannel == 0)
            {
                currentСhannel = 100;
            }
            Console.WriteLine(currentСhannel);
        }
        public void SwitchByNumber()
        {
        Start:
            try
            {

                Console.WriteLine("Enter, please number of chennel");
                numberOfChannel = Convert.ToInt32(Console.ReadLine());
                if (numberOfChannel > 100 || numberOfChannel < 1)
                {
                    Console.WriteLine("You have 100 channels");
                }
                else
                {
                    currentСhannel = numberOfChannel;
                    Console.WriteLine(currentСhannel);
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("You enter uncorrect value");
                goto Start;
            }
            catch(IndexOutOfRangeException)
            {
                Console.WriteLine("You have 100 channels!");
                goto Start;
            }
        }
        public void StopWatching()
        {
            Console.WriteLine("Good Buy");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TVset myTV = new TVset();
                Console.WriteLine("Hello. If you want to wath next channel - enter 1 ");
                Console.WriteLine("If you want to watch previous - enter 2");
                Console.WriteLine("Or if you want to watch channel by number - enter 3");
                Console.WriteLine("To stop watching - enter 4");
                int choose = Convert.ToInt32(Console.ReadLine());
                switch (choose)
                {
                    case 1:
                        myTV.SwitchNext();
                        break;
                    case 2:
                        myTV.SwitchPrevious();
                        break;
                    case 3:
                        myTV.SwitchByNumber();
                        break;
                    case 4:
                        myTV.StopWatching();
                        break;
                    default:
                        Console.WriteLine("You enter uncorrect value");
                        break;
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("You enter uncorrect value");
            }
            Console.ReadKey();

        }
    }
}
