﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DirectoryInfoClass
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 100; i++)
            {
                DirectoryInfo Folder_1 = Directory.CreateDirectory(@"Folder_" + i);
            }
            for (int i = 0; i < 100; i++)
            {
                Directory.Delete(@"Folder_" + i);
            }
            //var directory = new DirectoryInfo(@"C:\Windows\assembly");
            var directory = new DirectoryInfo(@".");
            if (directory.Exists)
            {
                Console.WriteLine("Full Name - {0}", directory.FullName);
                Console.WriteLine("Name - {0}", directory.Name);
                Console.WriteLine("Parent - {0}", directory.Parent);
                Console.WriteLine("CreationTime - {0}", directory.CreationTime);
                Console.WriteLine("Atributes - {0}", directory.Attributes.ToString());
                Console.WriteLine("Root - {0}", directory.Root);
                Console.WriteLine("LastAccessTime - {0}", directory.LastAccessTime);
                Console.WriteLine("LastWriteTime - {0}", directory.LastWriteTime);
            }
            else
            {
                Console.WriteLine("Directory does not exist");
            }
            FileInfo[] files = directory.GetFiles("*.exe");
            Console.WriteLine(files.Length);
            foreach (FileInfo file in files)
            {
                Console.WriteLine(file.Name);
                Console.WriteLine(file.Length);
                Console.WriteLine(file.CreationTime);
                Console.WriteLine(file.CreationTimeUtc);
                Console.WriteLine(file.Attributes.ToString());
            }
            directory.CreateSubdirectory("Subdir");
            directory.CreateSubdirectory(@"DirecoreInfoClass\SabDir");
            string[] drives = Directory.GetLogicalDrives();
            foreach (string drive in drives)
                Console.WriteLine(drive);

            //Files
            var newFile = new FileInfo(@".\Text.txt");
            FileStream stream = newFile.Create();
            Console.WriteLine(newFile.FullName);
            Console.WriteLine(newFile.CreationTime);
            stream.Close();

            Console.ReadKey();
        }
    }
}
