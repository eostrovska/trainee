﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CreateFile
{
    class Program
    {
        static void Main(string[] args)
        {
            var file = new FileInfo(@"Text.txt");
            StreamWriter stream = file.CreateText();
            stream.WriteLine("Line1");
            stream.WriteLine("Line2");
            Console.WriteLine(file.FullName);
            stream.Close();
            StreamReader reader = File.OpenText("Text.txt");
            string someString;
            while((someString = reader.ReadLine()) != null)
            {
                Console.WriteLine(someString);
            }
            reader.Close();
            Console.ReadLine();
        }
    }
}
