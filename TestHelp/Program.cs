﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {

            //Constants for limiting five-digit numbers
            const int maxFiveDigitNumber = 99999;
            const int minFiveDigitNumber = 10000;

            var primes = getPrimes(maxFiveDigitNumber);
            var maxKnownPalindrome = new PotentialPalindrome();
            long currentPalindrome;

            for (int i = 0; i < primes.Length; i++)
            {
                for (int j = 0; j < primes.Length; j++)
                {
                    //Condition for finding five-digit numbers
                    if (primes[i] >= minFiveDigitNumber && primes[j] >= minFiveDigitNumber)
                    {
                        currentPalindrome = (long)primes[i] * (long)primes[j];

                        //Condition for determining the polynodrome and storing it in a stack
                        if (isPalindrome(currentPalindrome) && currentPalindrome > maxKnownPalindrome.Palindrome)
                        {
                            maxKnownPalindrome.MultiplierOne = primes[i];
                            maxKnownPalindrome.MultiplierTwo = primes[j];
                            maxKnownPalindrome.Palindrome = currentPalindrome;
                        }
                    }
                }
            }

            Console.WriteLine("MultiplierOne: " + maxKnownPalindrome.MultiplierOne);
            Console.WriteLine("MultiplierTwo: " + maxKnownPalindrome.MultiplierTwo);
            Console.WriteLine("Max Palindrome: " + maxKnownPalindrome.Palindrome);

            Console.WriteLine("completed");
            Console.ReadKey();
        }

        public static bool isPalindrome(long potentialPalindrome)
        {
            var palindromeStr = potentialPalindrome.ToString();

            return palindromeStr == new string(palindromeStr.ToCharArray().Reverse().ToArray());
        }

        //Algorithm for finding prime numbers is the "Eratosthenes sieve"
        public static int[] getPrimes(int number)
        {
            var primes = Enumerable.Range(0, number + 1).ToArray();
            primes[1] = 0;
            for (int i = 2; i < primes.Length; ++i)
            {
                if (primes[i] != 0)
                {
                    for (int j = 2; i * j < primes.Length; ++j)
                    {
                        primes[i * j] = 0;
                    }
                }
            }
            return primes;
        }

        //Structure for storing potential palindromes
        public struct PotentialPalindrome
        {
            public int MultiplierOne;
            public int MultiplierTwo;
            public long Palindrome;
        }
    }
}
