﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorOverloading
{
    public class Money
    {
        public decimal Amount { get; set; }
        public string Unit { get; set; }

        public Money(decimal amount, string unit)
        {
            Amount = amount;
            Unit = unit;
        }
        public static Money operator +(Money a, Money b) //перегрузка оператора «+»
        {
            if (a.Unit != b.Unit)
                MoneyConvert.ToConvertMoney(b.Unit, a);

            return new Money(a.Amount + b.Amount, a.Unit);
        }
        public static bool operator ==(Money a, Money b)
        {
            if (a.Amount == b.Amount)
            {
                return true;
            }
            else { return false; }
        }
        public static bool operator !=(Money a, Money b)
        {
            if (a.Amount != b.Amount)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            return true;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
    public class MoneyConvert
    {
        public static void ToConvertMoney(string currency, Money a)
        {
            Console.WriteLine("Enter, please Exchange rate coefficient");
            int rate = Console.Read();
            if (currency == "UAH")
            {
                a.Amount = a.Amount * rate;
            }
            else
            {
                a.Amount = a.Amount / rate;
            }
            a.Unit = currency;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Money myMoney = new Money(100, "USD");
            Money yourMoney = new Money(100, "UAN");
            Money hisMoney = new Money(50, "USD");
            Money sum = myMoney + hisMoney;
            sum = yourMoney + hisMoney;
            Console.WriteLine(sum.Amount + sum.Unit);
            if (myMoney.Amount == yourMoney.Amount)
            {
                Console.WriteLine("true");
            }
            else
            {
                Console.WriteLine("false");
            }
            Console.ReadKey();
        }
    }
}
