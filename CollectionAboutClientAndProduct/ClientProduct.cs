﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionAboutClientAndProduct
{
    class ClientProduct
    {
        Dictionary<string, string[]> clientsData = new Dictionary<string, string[]>();
        public void AddProductCategoryAndClients(string productCategory, string[] clients)
        {
            clientsData.Add(productCategory,clients);
        }
        public string[] ShowClientsOfProductCategory(string productCategory)
        {
            string[] clients = new string[0];
            foreach (KeyValuePair<string, string[]> item in clientsData)
            {
                if(item.Key == productCategory)
                {
                    clients = item.Value;
                    foreach(string i in clients)
                    {
                        Console.WriteLine(i);
                    }
                }
            }
            return clients;
        }
        public string ShowProductCategoryOfClients(string[] clients)
        {
            string productCategory = "";
            foreach (KeyValuePair<string,string[]> item in clientsData)
            {
                if(item.Value == clients)
                {
                    Console.WriteLine(item.Key);
                    productCategory = item.Key;
                }
            }
            return productCategory;
        }
    }
}
