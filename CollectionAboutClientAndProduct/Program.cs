﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionAboutClientAndProduct
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] honneyClients = new string[] { "Ivan", "Danil" };
            ClientProduct clientsData = new ClientProduct();
            clientsData.AddProductCategoryAndClients("Honney", honneyClients);
            clientsData.ShowClientsOfProductCategory("Honney");
            clientsData.ShowProductCategoryOfClients(honneyClients);
            Console.ReadKey();
        }
    }
}
