﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
namespace httpwebrequest_i_httpwebresponse
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.GetEncoding(1251);
            string uri = "http://mycsharp.ru/";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
            Regex myReg = new Regex("(?<=color=\"\\#454545\" size=\"6\">)([^<]+)");
            MatchCollection matches = myReg.Matches(reader.ReadToEnd());
            foreach (Match m in matches)
                Console.WriteLine(m.Value);
            Console.ReadLine();
        }
    }
}
