﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Collections
{
    public class Elements
    {
        public int Field1 { get; set; }
        public int Field2 { get; set; }
        public Elements(int field1, int field2)
        {
            Field1 = field1;
            Field2 = field2;
        }
    }
}
