﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Collections
{
    class Program
    {
        static void Main()
        {
            var collection = new UserCollection();

            collection[0] = new Elements(1, 1);
            collection[1] = new Elements(1, 2);
            collection[2] = new Elements(1, 3);
            collection[3] = new Elements(1, 4);

            foreach(Elements element in collection)
            {
                Console.WriteLine("{0}, {1}", element.Field1, element.Field2);
            }
            Console.ReadKey();
        }
    }
}
