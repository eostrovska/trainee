﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ArrayListSort
{
    class Program
    {
        static void Main(string[] args)
        {
            var arrayList = new ArrayList { 6, 7, 4, };
            arrayList.Sort(new DescendingComparer());
            foreach (int item in arrayList)
                Console.WriteLine(item);
            Console.ReadKey();
        }
    }
    public class DescendingComparer : IComparer
    {
        readonly CaseInsensitiveComparer comparer = new CaseInsensitiveComparer();
        public int Compare(object x, object y)
        {
            int result = -1 * comparer.Compare(x, y);
            return result;
        }
    }
}
