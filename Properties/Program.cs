﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{
    class TVset
    {
        private int volume;
        public int Volume
        {
            get { return volume; }
            set
            {
                if (value < 1 || value > 100)
                {
                    Console.WriteLine("You entered uncorrect volume. Min volume is 1. Max volume is 100.");
                    volume = 1;
                }
                else volume = value;
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            TVset myTVVolume = new TVset();
            myTVVolume.Volume = 4;
            Console.WriteLine(myTVVolume.Volume);
            myTVVolume.Volume = 109;
            Console.WriteLine(myTVVolume.Volume);
            myTVVolume.Volume = 0;
            Console.WriteLine(myTVVolume.Volume);
            Console.ReadKey();
        }
    }
}
