﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodsOverload
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = new string[] { "Petr", "Ivan", "Alex", "Max", "Victor", "Helen", "Helga", "Georg", "Mina" };
            Program students = new Program();
            students.ShowNames(names);
            students.ShowNames(names, '/');
            students.ShowNames(names, "jjjj");
            Console.ReadLine();
        }

        public void ShowNames(string[] name)
        {
            foreach (string someName in name)
            {
                Console.Write("{0}", someName);
            }
            Console.WriteLine();
        }
        public void ShowNames(string[] name, char separator)
        {
            foreach (string someName in name)
            {
                Console.Write("{0}{1}", someName, separator);
            }
            Console.WriteLine();
        }
        public void ShowNames(string[] name, string someSeparator)
        {
            foreach (string someName in name)
            {
                Console.Write("{0}{1}", someName, someSeparator);
            }
        }
    }
}


