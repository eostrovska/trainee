﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Threads
{
    class Program
    {
        static void SomeThread()
        {
            Console.WriteLine("Second thread");
            Console.WriteLine(Thread.CurrentThread.GetHashCode());
            for (int i = 0; i < 300; i++)
            {
                Thread.Sleep(40);
                Console.Write(":");
            }
            Console.WriteLine("Second thread ended");
        }
        static void Main(string[] args)
        {
            var thread = new Thread(SomeThread);
            thread.Start();
            Console.WriteLine(Thread.CurrentThread.GetHashCode());
            thread.Join();
            for (int i = 0; i < 18; i++)
            {
                Thread.Sleep(40);
                Console.Write("--");
            }
            Console.WriteLine("First thread ended");
            Console.ReadKey();
        }



        //private static void Function()
        //{
        //for (int i = 0; i < 500; i++)
        //{
        //Thread.Sleep(10);
        //Console.Write("+");
        //}
        //Console.WriteLine("Third thread ended");
        //}
        //static void Main(string[] args)
        //{
        //var thread = new Thread(Function);
        //thread.IsBackground = true;
        //thread.Start();
        //Thread.Sleep(500);
        //Console.WriteLine("First thread ended");
        //}
    }
}
