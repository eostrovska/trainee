﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualMethod
{
    class Warrior
    {
        protected internal int lives = 100;
        protected internal int armor = 0;
        public int Lives { get { return lives; } set { lives = value; } }
        public virtual int TakeDamage(int damage)
        {
            Lives = Lives + armor - damage;
            return Lives;
        }
    }
    class WarriorWithLightArmor : Warrior
    {
        public override int TakeDamage(int damage)
        {
            armor = 10;
            return base.TakeDamage(damage);
        }
    }
    class WarriorWithHightArmor : Warrior
    {
        public override int TakeDamage(int damage)
        {
            armor = 30;
            return base.TakeDamage(damage);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            int healtheDamage = 30;
            Warrior warrior1 = new Warrior();
            WarriorWithHightArmor warrior2 = new WarriorWithHightArmor();
            WarriorWithLightArmor warrior3 = new WarriorWithLightArmor();
            Console.WriteLine(warrior1.TakeDamage(healtheDamage));
            Console.WriteLine(warrior2.TakeDamage(healtheDamage));
            Console.WriteLine(warrior3.TakeDamage(healtheDamage));
            Console.ReadKey();
        }
    }
}
