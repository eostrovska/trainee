﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enums
{
    enum Food
    {
        Fish = 10,
        Mouse = 8,
        Feed = 6,
        Milk = 3,
        SourCream = 5
    };
    class Cat
    {

        public int SatietyLevel
        {
            get; set;
        }
        public void EatSomething(int food)
        {
            SatietyLevel = food;
            Console.WriteLine("Your cat ate and has {0}th satiety level", SatietyLevel);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string nameOfCat = "Parlament";
            Cat myCat = new Cat();
            myCat.EatSomething((int)Food.Feed);
            Console.WriteLine("It's name is {0}", nameOfCat);
            Console.ReadKey();
        }
    }
}
