﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestToITAcademy
{
    class Pets
    {
        private string name;
        private string breed;
        public Pets(string name, string breed)
        {
            this.name = name;
            this.breed = breed;
        }
        public void Introduce()
        {
            Console.WriteLine("I'm {0} of {1}.I'm a {2}", name, breed, GetType().Name);
        }
    }
    class Petshop
    {
        List<Pets> pets = new List<Pets>();
        public void AddPet(Pets animal)
        {
            pets.Add(animal);
        }
        public void IntroduceAll()
        {
            foreach (Pets pet in pets)
                pet.Introduce();
        }
    }
    class Dog : Pets
    {
        public Dog(string name, string breed) : base(name, breed) { }
    }
    class Cat : Pets
    {
        public Cat(string name, string breed) : base(name, breed) { }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Petshop petshop = new Petshop();
            petshop.AddPet(new Cat("Mira", "HomeCat"));
            petshop.AddPet(new Cat("Duck","HomeCat"));
            petshop.AddPet(new Dog("Reeck","Doberman"));
            petshop.AddPet(new Dog("Fozzy","Taksa"));
            petshop.IntroduceAll();
            Console.ReadKey();
        }
    }
}
//Create next classes: Cat, Dog and Petshop.
//Types Cat and Dog have fields Name and Breed and method Introduce() which prints text “I’m(Name) of(Breed). I’m a cat(or dog)”.
//The class Petshop collects in its container different pets.
//We may add new pet to container by method AddPet() and we may display information about all pets by method IntroduceAll().
//Which hierarchy of classes is the best solution of this problem?
//Write short code to demonstrate your solution. Your code should include class (interface) aggregation, inheritance, 
//should use.NET BCL collections or generics, should implement exception handling.
