﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringFormat
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] temperature = new double[3] { 26.3, 27.1, 30 };
            DateTime[] dates = new DateTime[3];
            for (int s = 0; s < 3; s++)
            {
                dates[s] = DateTime.Now;
            }
            for (int i = 0; i < dates.Length && i < temperature.Length; i++)
            {

                string formatDates = String.Format(@"{0:MMM dd \(ddd)\ H:mm >}", dates[i]);
                string formatTemperature = String.Format(@"{0:f2}", temperature[i]);
                Console.WriteLine("{0} {1} C°", formatDates, formatTemperature);
                {
                    Console.WriteLine("Index Out Of Range Exception");
                }
            }
            Console.ReadKey();
        }
    }
}
