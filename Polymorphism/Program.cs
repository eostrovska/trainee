﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    interface ICarpetCleaning
    {
        void MyCarpetClerning();
    }
    class CarpetClearningByVAcuumCleaner : ICarpetCleaning
    {
        public void MyCarpetClerning()
        {
            Console.WriteLine("Clearning with vacuum cleaner");
        }
    }
    class CarpetClearningByBroom : ICarpetCleaning
    {
        public void MyCarpetClerning()
        {
            Console.WriteLine("Clearning with broom");
        }
    }
    class CarpetClearningByWashingVacuumCleaner : ICarpetCleaning
    {
        public void MyCarpetClerning()
        {
            Console.WriteLine("Clearning with washing vacuum cleaner");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            CarpetClearningByVAcuumCleaner cleaner1 = new CarpetClearningByVAcuumCleaner();
            CarpetClearningByBroom cleaner2 = new CarpetClearningByBroom();
            CarpetClearningByWashingVacuumCleaner cleaner3 = new CarpetClearningByWashingVacuumCleaner();
            cleaner1.MyCarpetClerning();
            cleaner2.MyCarpetClerning();
            cleaner3.MyCarpetClerning();
            Console.ReadKey();
        }
    }
}
