﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstructClassAndMethod
{
    abstract class Human
    {
        public string Name { get; set; }
        public string Nationality { get; set; }
        public Human(string name, string nationality)
        {
            this.Name = name;
            this.Nationality = nationality;
        }
        public abstract void Greeting();

    }
    class Ukrainian : Human
    {
        public Ukrainian(string name, string nationality) : base(name, nationality) { }
        public override void Greeting()
        {
            Console.WriteLine("{0} is {1} and say Привiт", Name, Nationality);
        }
    }
    class Russian : Human
    {
        public Russian(string name, string nationality) : base(name, nationality) { }
        public override void Greeting()
        {
            Console.WriteLine("{0} is {1} and say Привет", Name, Nationality);
        }
    }
    class American : Human
    {
        public American(string name, string nationality) : base(name, nationality) { }
        public override void Greeting()
        {
            Console.WriteLine("{0} is {1} and say Hello", Name, Nationality);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.GetEncoding(1251);
            Ukrainian ukrainian = new Ukrainian("Taras", "ukrainian");
            Russian russian = new Russian("Viktor", "russian");
            American american = new American("Alex", "american");
            ukrainian.Greeting();
            russian.Greeting();
            american.Greeting();
            Console.ReadLine();
        }
    }
}
