﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YeldMagic
{
    class Program
    {
        static void Main()
        {
            var collection = new UserCollection<Elements>();
            collection[0] = new Elements(12, 2, 3, 4);
            collection[1] = new Elements(12, 2, 3, 4);
            collection[2] = new Elements(12, 2, 3, 4);
            collection[3] = new Elements(12, 2, 3, 4);
            foreach (Elements element in collection)
            {
                Console.WriteLine("{0}, {1}, {2}, {3}", element.F1, element.F2, element.F3, element.F4);
            }
            foreach (Elements element in collection)
            {
                Console.WriteLine("{0}, {1}, {2}, {3}", element.F1, element.F2, element.F3, element.F4);
            }
            Console.ReadKey();
        }
    }
}
