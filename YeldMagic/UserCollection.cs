﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace YeldMagic
{
    class UserCollection<T>
    {
        T[] collection = new T[4];
        public T this[int index]
        {
            get { return collection[index]; }
            set { collection[index] = value; }
        }
        int position = -1;
        void Reset()
        {
            position = -1;
        }
        public IEnumerator GetEnumerator()
        {
            foreach (var element in collection)
            {
                yield return element;
            }
        }
    }
}
