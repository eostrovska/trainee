﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace WorkingWithFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            var file = new FileInfo(@"Text.txt");
            StreamWriter stream = file.CreateText();
            stream.WriteLine("Line1");
            stream.WriteLine("Line2");
            Console.WriteLine(file.FullName);
            stream.Close();
            string fileF = @"Text.txt";
            Console.WriteLine(Path.GetFullPath(fileF));
            Console.WriteLine(Path.GetFileName(@"C:\Projects\Trainy\CreateFile\bin\Debug\Text.txt"));
            DriveInfo[] findingFile = DriveInfo.GetDrives();
            foreach (DriveInfo drive in findingFile)
            {
                Console.WriteLine(drive.Name);
            }
            try
            {
                string[] dirs = Directory.GetFiles(@"C:\", "Text.txt");
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
            StreamReader reader = File.OpenText("Text.txt");
            string someString;
            while ((someString = reader.ReadLine()) != null)
            {
                Console.WriteLine(someString);
            }
            reader.Close();
            FileStream source = File.OpenRead(@"Text.txt");
            FileStream destination = File.Create(@"archive.zip");
            GZipStream compressor = new GZipStream(destination, CompressionMode.Compress);
            int theByte = source.ReadByte();
            while(theByte != -1)
            {
                compressor.WriteByte((byte)theByte);
                theByte = source.ReadByte();
            }
            compressor.Close();
            Console.ReadKey();
        }
    }
}
