﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EqualsAndGetHashCodeMethods
{
    class Circle
    {
        //private int x;
        //private int y;
        //private int radius;
        public int X { get; set; }
        public int Y { get; set; }
        public int Radius { get; set; }
        public Circle(int x, int y, int radius)
        {
            this.X = x;
            this.Y = y;
            this.Radius = radius;
        }
        public bool Equals(Circle o)
        {
           if(o == null)
            return false;
            return this.X == o.X && this.Y == o.Y && this.Radius == o.Radius; 
        }
        public override int GetHashCode()
        {
            string s = string.Format("{0}{1}{2}", Radius, X, Y);

            return Convert.ToInt32(s);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Circle circle1 = new Circle(12, 12, 12);
            Circle circle2 = new Circle(12,12,12);
            Circle circle3 = new Circle(12,12,13);
            Console.WriteLine(circle1.Equals(circle2));
            Console.WriteLine(circle2.Equals(circle3));
            Console.WriteLine(circle1.GetHashCode());
            Console.WriteLine(circle2.GetHashCode());
            Console.WriteLine(circle3.GetHashCode());
            Console.ReadKey();
        }
    }
}
