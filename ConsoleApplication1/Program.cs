﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bases_Constructor
{
    class GeometricFigure
    {
        private int x;
        private int y;
        public GeometricFigure(int x, int y)
        {
            this.x = x;
            this.y = y;
            Console.WriteLine("Coordinates of the center of the figure are {0} and {1}", x, y);
        }
    }
    class Circle : GeometricFigure
    {
        private int radius;
        public Circle(int x, int y, int radius) : base(x, y)
        {
            this.radius = radius;
        }
        public void PaintCircle()
        {
            Console.WriteLine("You have take compass and measure radius of a long {0}. Then paint rhe circle.", radius);
        }
    }
    class Triangle : GeometricFigure
    {
        private int perimeter;
        private int firstSide;
        private int secondSide;
        private int thirdSide;
        public Triangle(int x, int y, int firstSide, int secondSide, int thirdSide) : base(x, y)
        {
            this.firstSide = firstSide;
            this.secondSide = secondSide;
            this.thirdSide = thirdSide;
        }
        public void PaintTriangle()
        {
            //perimeter = firstSide + secondSide + thirdSide;
            Console.WriteLine("Triangle has three side - {0}cm, {1}cm, {2}cm. It's perimeter is {3}cm.", firstSide, secondSide, thirdSide, perimeter);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Circle myCircle = new Circle(2, 3, 46);
            myCircle.PaintCircle();
            Triangle myTriangle = new Triangle(3, 4, 4, 5, 6);
            myTriangle.PaintTriangle();
            Console.ReadKey();
        }
    }
}
