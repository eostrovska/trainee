﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operator
{
    class Money
    {
        public double Amount { get; set; }
        public string Unit { get; set; }

        public Money(double amount, string unit)// конструктор
        {
            Amount = amount;
            Unit = unit;
        }

        public static Money operator +(Money a, Money b)// делаем перегрузку оператора "+"
        {
            if (a.Unit.Contains("USD") || b.Unit.Contains("USD"))// проверяем содержит ли строка подстроку "USD"
            {
                return new Money((a.Amount * 65.55) + b.Amount, "USD");// если у нас баксы, делаем пересчет по курсу ЦБ
            }
            else
            {
                return new Money(a.Amount + b.Amount, "USD");
            }
        }

        public static bool operator ==(Money a, Money b)// перегружаем оператор сравниния
        {
            if (a.Amount == b.Amount)
                return true;
            return false;
        }

        public static bool operator !=(Money a, Money b)//перегружаем оператор сравнения
        {
            if (a.Amount != b.Amount)
                return true;
            return false;
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            return true;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Money myMoney = new Money(100, "USD");
            Money yourMoney = new Money(100, "RUR");
            Money hisMoney = new Money(50, "USD");
            Money sum = myMoney + yourMoney;//складываем используя перегруженный оператор сложения
            Console.WriteLine(sum.Amount + " " + sum.Unit);

            if (myMoney.Amount == yourMoney.Amount)// делаем проверку на равенство
            {
                Console.WriteLine("Значения одинаковые");
            }
            else
            {
                Console.WriteLine("Значения отличаются");
            }
            Console.ReadKey();
        }
    }
}
