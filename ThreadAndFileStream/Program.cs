﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace ThreadAndFileStream
{
    class Program
    {
        static void SecondThread()
        {
            string[] lines = File.ReadAllLines("1.txt");
            var destination = new FileInfo("destination.txt");
            StreamWriter stream = destination.CreateText();
            foreach (string item in lines)
            { stream.WriteLine(item); }
            stream.Close();
        }
        static void Main(string[] args)
        {
            var thread = new Thread(SecondThread);
            thread.Start();
            string[] linesMain = File.ReadAllLines("2.txt");
            string destinationMain = @"C:\Projects\Trainy\ThreadAndFileStream\bin\Debug\destination.txt";
            thread.Join();
            File.AppendAllLines(destinationMain, linesMain, Encoding.UTF8);
        }
    }
}