﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace StreamReader_StreamWriter
{
    class Program
    {
        static void Main(string[] args)
        {
            //task1
            FileStream file1 = new FileStream("C://Projects//numbers.txt", FileMode.Create);
            StreamWriter writer = new StreamWriter(file1);
            for (int i = 1; i <= 500; i++)
            {
                writer.Write("{0}, ", i);
            }
            writer.Close();

            //task2
            string[] colour = new string[5] { "red", "green", "black", "white", "blue" };
            FileStream fiel2 = new FileStream("C://Projects//colours.txt", FileMode.Create);
            StreamWriter colours = new StreamWriter(fiel2);
            for(int g = 1; g<colour.Length; g++)
            {
                colours.WriteLine(colour[g]);
            }
            colours.Close();
            Console.ReadKey();
        }
    }
}
