﻿#define VS_2015
using System;

namespace Less18_task05
{
    class Program
    {
        static void Main(string[] args)
        {
#if (DEBUG && VS_2015)
            Console.WriteLine("debug on 2015");
#elif (DEBUG && VS_2012)
			Console.WriteLine("debug on 2012");
#elif (!DEBUG && VS_2015)
			Console.WriteLine("release on 2015");
#elif (!DEBUG && VS_2012)
			Console.WriteLine("release on 2012");
#endif
        }
    }
}
