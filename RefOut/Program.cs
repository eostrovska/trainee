﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefOut
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 5;
            Console.Write(i);
            Foo(ref i);
            Console.Write(i);
            Console.ReadLine();
        }
        private static void Foo (ref int i)
        {
            i++;
            Console.Write(i);
        }
    }
}
