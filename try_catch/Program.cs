﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace try_catch
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int[] array = new int[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                int index1, index2;
                Console.WriteLine("Enter, please first index");
                index1 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter, please second index");
                index2 = Convert.ToInt32(Console.ReadLine());
                int result = array[index1] + array[index2];
                Console.WriteLine(result);
            }
            catch(FormatException)
            {
                Console.WriteLine("You entered uncorrect value!");
            }
            catch(IndexOutOfRangeException)
            {
                Console.WriteLine("You entered index that is outside the collection");
            }
            Console.ReadKey();
        }
    }
}
