﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NullableTypes
{
    class Program
    {
        public string Name { get; set; }
        public void Info(string name, int? children)
        {
            Name = name;
            if (children == null)
            {
                Console.WriteLine("We don't know how many children has {0}", Name);
            }

            else if (children == 0)
            {
                Console.WriteLine("{0} hasn't children.", name);
            }
            else
            {
                Console.WriteLine("{0} has {1} child/children", Name, children);
            }
        }
        static void Main(string[] args)
        {
            Program myInfo = new Program();
            Console.WriteLine("Enter, please your name");
            string name = Console.ReadLine();
            int? children;
            try
            {
                Console.WriteLine("Enter, please how many children you have?");
                children = Convert.ToInt32(Console.ReadLine());
            }
            catch { children = null; }
            myInfo.Info(name, children);
            Console.ReadKey();
        }
    }
}
