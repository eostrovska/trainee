﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace CultureInfoClass
{
    class Program
    {
        static void Main(string[] args)
        {
            CultureInfo current = CultureInfo.CurrentCulture;
            Console.WriteLine(current);
            CultureInfo[] cultureInfo = CultureInfo.GetCultures(CultureTypes.AllCultures);
            Console.WriteLine(cultureInfo.Length);
            foreach(CultureInfo c in cultureInfo)
            {
                Console.WriteLine("{0} | {1}", c.EnglishName, c.ToString());
            }
            RegionInfo regions = RegionInfo.CurrentRegion;
            Console.WriteLine(regions);
            Console.WriteLine(regions.NativeName);
            Console.WriteLine(regions.EnglishName);
            Console.WriteLine(regions.CurrencyEnglishName);
            Console.WriteLine(regions.CurrencyNativeName);
            Console.WriteLine(regions.CurrencySymbol);
            string[] days = CultureInfo.CurrentCulture.DateTimeFormat.DayNames;
            foreach(string day in days)
            {
                Console.WriteLine(day);
            }
            string [] deDays = CultureInfo.GetCultureInfo("el-CY").DateTimeFormat.DayNames;
            foreach(string day in deDays)
            {
                Console.WriteLine(day);
            }
            Console.ReadKey();
        }
    }
}
