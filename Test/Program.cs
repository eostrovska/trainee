﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        public static int[] FillSieve(int n)
        {
            var primes = Enumerable.Range(0, n + 1).ToArray();
            primes[1] = 0;
            for (int i = 2; i < primes.Length; ++i)
            {
                if (primes[i] != 0)
                {
                    for (int j = 2; i * j < primes.Length; ++j)
                    {
                        primes[i * j] = 0;
                    }
                }
            }
            return primes;
        }
        static int ObtratnoeChislo(int n)
        {
            int obr = 0;
            while (n > 0)
            {
                obr = 10 * obr + n % 10;
                n /= 10;
            }
            return obr;
        }

        static void Main(string[] args)
        {
            var po = FillSieve(99999);
            po.ToString();
            Console.WriteLine(po);
            //var primes = FillSieve(99999).Where(n => n > 9999).Select(n => (ulong)n);
            //primes.ToString();
            //Console.WriteLine(primes);
            Console.ReadKey();
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 
namespace ConsoleApplication1
{
    class Prosto
    {
        public bool isPrime(int x)
        {
            for (int i = 2; i < x / 2 + 1; i++)
            {
                if ((x % i) == 0) return false;
            }
            return true;
        }
    }
    class Program
    {
        static int ObtratnoeChislo(int n)
        {
            int obr = 0;
            while (n > 0)
            {
                obr = 10 * obr + n % 10;
                n /= 10;
            }
            return obr;
        }
        static void Main(string[] args)
        {
            int otv = 0;
            Prosto pr = new Prosto();
            for (int i = 99000; i < 100000; i++)
            {
                for (int j = 99000; j < 100000; j++)
                {
                    if (pr.isPrime(i) && pr.isPrime(j))
                    {
                        otv = i * j;
                        if (otv == ObtratnoeChislo(otv))
                        {
                            Console.WriteLine(i + " " + j);
                            break;
                        }
                        else otv = 0;
                    }
                }
                if (otv != 0) break;
            }
            Console.WriteLine(otv);//99041 99793, 1293663921
            Console.ReadKey();
        }
        
    }
    
}
*/
