﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackOverflow
{
    
    class Program
    {
        
            public static void Method()
            {
                int[] array = new int[10];
                    Console.WriteLine(array);
                    Method();
            }
        
        static void Main(string[] args)
        {
            try
            {
                Method();
            }
            catch(Exception)
            {
                Console.WriteLine("StackOverflowException");
            }
            finally
            {
                Console.WriteLine("Finally");
            }
        }
    }
}
