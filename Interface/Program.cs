﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    interface ISwitchable
    {
        void SwitchOn();
        void SwitchOff();
    }
    class Computer : ISwitchable
    {
        public int Time { get; set; }
        public Computer(int time)
        {
            this.Time = time;
            if (time > 20)
            {
                SwitchOn();
            }
            else
            {
                SwitchOff();
            }
        }
        public void SwitchOn()
        {
            Console.WriteLine("Time is {0}. Wake up!", Time);
        }
        public void SwitchOff()
        {
            Console.WriteLine("Time is {0}. It is time go to bed!", Time);
        }
    }
    class Conditioner : ISwitchable
    {
        public int Temperature { get; set; }
        public Conditioner(int temperature)
        {
            this.Temperature = temperature;
            if (temperature > 20)
            {
                SwitchOn();
            }
            else
            {
                SwitchOff();
            }
        }
        public void SwitchOn()
        {
            Console.WriteLine("The temperature is {0}. It is so hot. Conditioner help you!", Temperature);
        }
        public void SwitchOff()
        {
            Console.WriteLine("The temperature is {0}. It is cold. You need not working conditioner.", Temperature);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Computer myComputer = new Computer(45);
            Conditioner myConditioner = new Conditioner(5);
            Console.ReadLine();
        }
    }
}
