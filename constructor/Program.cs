﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace constructor
{
    class Student
    {
        private string name;
        private int course;
        private bool grants;
        public Student(string name, int course, bool grants)
        {
            this.name = name;
            this.course = course;
            this.grants = grants;
        }
        public void Info()
        {
            Console.WriteLine("Name of student - {0}.", name);
            Console.WriteLine("Student's course - {0}", course);
            Console.WriteLine("Student has grants - {0}", grants);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Student student1 = new Student("Helen", 4, true);
            Student student2 = new Student("Mark", 3, false);
            Student student3 = new Student("Rome", 4, true);
            student1.Info();
            student2.Info();
            student3.Info();
            Console.ReadKey();
        }
    }
}
